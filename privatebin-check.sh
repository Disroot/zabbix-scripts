#!/bin/
#This script checks the amount of bin and amount of connected people
source /var/local/container-scripts/zabbix-scripts.cfg

#statistics
BIN_TOTAL=$(sudo -u ${BIN_USER} find ${BIN_PATH} -mindepth 2 -name '*.php' | wc -l)
BIN_SIZE=$(du -bs ${BIN_PATH} | cut -f1)

echo "bin_total ${BIN_TOTAL}" > /tmp/bin_stats
echo "bin_size ${BIN_SIZE}" >> /tmp/bin_stats

