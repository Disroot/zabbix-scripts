#!/bin/bash
#This script checks the amount of calcs and amount of connected people
source /var/local/container-scripts/zabbix-scripts.cfg

#statistics
PAD_TOTALPADS=$(mysql -h "${PAD_HOST}" -u "${PADDB_USER}" -p"${PADDB_PASSWD}" "${PADDB_NAME}" -s -r -N -e \
  "select count(distinct substring(store.key,5,locate(':',store.key,5)-5)) as 'pads' from store where store.key like 'pad:%';")
PAD_ACTIVEUSERS=$(curl http://${PAD_LISTEN}:${PAD_PORT}/stats |jq .totalUsers)
PAD_ACTIVEPADS=$(curl http://${PAD_LISTEN}:${PAD_PORT}/stats |jq .activePads)

echo "pad_totalpads ${PAD_TOTALPADS}" > /tmp/pad_stats
echo "pad_activeusers ${PAD_ACTIVEUSERS}" >> /tmp/pad_stats
echo "pad_activepads ${PAD_ACTIVEPADS}" >> /tmp/pad_stats
