#!/bin/bash
#This script checks the amount of calcs and amount of connected people
source /var/local/container-scripts/zabbix-scripts.cfg

#all calcs
CALC_ALL=$(redis-cli KEYS "snapshot-*" |wc -l)

#save to a file
echo "calc_all ${CALC_ALL}" > /tmp/ethercalc_stats
