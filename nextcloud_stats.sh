#!/bin/bash
#This is script that collects nextlcoud statistics
source /var/local/container-scripts/zabbix-scripts.cfg

#!/bin/bash
curl -s https://"${NC_ADMIN_USER}":"${NC_ADMIN_PW}"@"${NC_HOST}"/ocs/v2.php/apps/serverinfo/api/v1/info > /tmp/cloudstats
NC_DECK_BOARDS=$(mysql -h "${CLOUDDB_HOST}" -u "${CLOUDDB_USER}" -p"${CLOUDDB_PASSWORD}" "${CLOUDDB_NAME}" -s -r -N -e "select count(*) from ${NC_PREFIX}deck_boards;")
NC_FORMS=$(mysql -h "${CLOUDDB_HOST}" -u "${CLOUDDB_USER}" -p"${CLOUDDB_PASSWORD}" "${CLOUDDB_NAME}" -s -r -N -e "select count(*) from ${NC_PREFIX}forms_v2_forms;")
NC_TALK_ROOMS=$(mysql -h "${CLOUDDB_HOST}" -u "${CLOUDDB_USER}" -p"${CLOUDDB_PASSWORD}" "${CLOUDDB_NAME}" -s -r -N -e "select count(*) from ${NC_PREFIX}talk_rooms where last_activity is not null;")
NC_CIRCLES=$(mysql -h "${CLOUDDB_HOST}" -u "${CLOUDDB_USER}" -p"${CLOUDDB_PASSWORD}" "${CLOUDDB_NAME}" -s -r -N -e "select count(*) from ${NC_PREFIX}circle_circles;")
NC_CALENDARS=$(mysql -h "${CLOUDDB_HOST}" -u "${CLOUDDB_USER}" -p"${CLOUDDB_PASSWORD}" "${CLOUDDB_NAME}" -s -r -N -e "select count(*) from ${NC_PREFIX}calendars;")
NC_CONTACTS=$(mysql -h "${CLOUDDB_HOST}" -u "${CLOUDDB_USER}" -p"${CLOUDDB_PASSWORD}" "${CLOUDDB_NAME}" -s -r -N -e "select count(*) from ${NC_PREFIX}cards;")
NC_BOOKMARKS=$(mysql -h "${CLOUDDB_HOST}" -u "${CLOUDDB_USER}" -p"${CLOUDDB_PASSWORD}" "${CLOUDDB_NAME}" -s -r -N -e "select count(*) from ${NC_PREFIX}bookmarks;")
NC_COSPEND=$(mysql -h "${CLOUDDB_HOST}" -u "${CLOUDDB_USER}" -p"${CLOUDDB_PASSWORD}" "${CLOUDDB_NAME}" -s -r -N -e "select count(*) from ${NC_PREFIX}cospend_projects;")
NC_APPOINTMENTS=$(mysql -h "${CLOUDDB_HOST}" -u "${CLOUDDB_USER}" -p"${CLOUDDB_PASSWORD}" "${CLOUDDB_NAME}" -s -r -N -e "select count(*) from ${NC_PREFIX}appointments_hash;")

echo "nc_deck_boards " "${NC_DECK_BOARDS}" >> /tmp/nc_stats
echo "nc_talk_rooms" "${NC_TALK_ROOMS}" >> /tmp/nc_stats
echo "nc_circles" "${NC_CIRCLES}" >> /tmp/nc_stats
echo "nc_calendars" "${NC_CALENDARS}" >> /tmp/nc_stats
echo "nc_contacts" "${NC_CONTACTS}" >> /tmp/nc_stats
echo "nc_bookmarks" "${NC_BOOKMARKS}" >> /tmp/nc_stats
echo "nc_cospend" "${NC_COSPEND}" >> /tmp/nc_stats
echo "nc_appointments" "${NC_APPOINTMENTS}" >> /tmp/nc_stats
