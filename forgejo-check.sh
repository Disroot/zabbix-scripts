
#!/bin/bash
#This script checks the amount of users and repos
source /var/local/container-scripts/zabbix-scripts.cfg

#users
GIT_USERS=$(psql postgresql://${GITDB_USER}:${GITDB_PASSWD}@${GITDB_HOST}:${GITDB_PORT}/${GITDB_NAME} -t -c "SELECT COUNT(*) FROM \"user\";")

#all repos
GIT_ALLREPOS=$(psql postgresql://${GITDB_USER}:${GITDB_PASSWD}@${GITDB_HOST}:${GITDB_PORT}/${GITDB_NAME} -t -c "SELECT COUNT(repository) FROM repository;")

#public repos
GIT_PUBREPOS=$(psql postgresql://${GITDB_USER}:${GITDB_PASSWD}@${GITDB_HOST}:${GITDB_PORT}/${GITDB_NAME} -t -c "SELECT COUNT(repository) FROM repository WHERE is_private = 'f';")

#private repos
GIT_PRIVREPOS=$(psql postgresql://${GITDB_USER}:${GITDB_PASSWD}@${GITDB_HOST}:${GITDB_PORT}/${GITDB_NAME} -t -c "SELECT COUNT(repository) FROM repository WHERE is_private != 'f';")

#10 biggest repo
GIT_BIGGESTREPOS=$(psql postgresql://${GITDB_USER}:${GITDB_PASSWD}@${GITDB_HOST}:${GITDB_PORT}/${GITDB_NAME} -t -c "SELECT name,PG_SIZE_PRETTY(size),owner_name FROM repository ORDER BY size DESC LIMIT 10;" | sed 's/|//g')

#10 repo owners
GIT_REPOCOUNT=$(psql postgresql://${GITDB_USER}:${GITDB_PASSWD}@${GITDB_HOST}:${GITDB_PORT}/${GITDB_NAME} -t -c "SELECT owner_name,count(*) AS repo_count FROM repository GROUP BY owner_name ORDER BY repo_count DESC LIMIT 10;" | sed 's/|//g') 

#save onto a file
echo "git_users ${GIT_USERS}" > /tmp/git_stats
echo "git_allrepos ${GIT_ALLREPOS}" >> /tmp/git_stats
echo "git_pubrepos ${GIT_PUBREPOS}" >> /tmp/git_stats
echo "git_privrepos ${GIT_PRIVREPOS}" >> /tmp/git_stats
echo "git_biggestrepos ${GIT_BIGGESTREPOS}" >> /tmp/git_stats
echo "git_repocount" ${GIT_REPOCOUNT} >> /tmp/git_stats
