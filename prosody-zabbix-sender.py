#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2010 Christoph Heer (Christoph.Heer@googlemail.com)
# Copyright (c) 2015 Tim Schumacher (tim@datenknoten.me)
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the \"Software\"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

import sys
import os
import asyncio
import re
from subprocess import Popen, PIPE, STDOUT
import urllib.parse
from pprint import pprint

server = sys.argv[1]
host = sys.argv[2]

config = {
    'zabbix': {
        'sender': '/usr/bin/zabbix_sender',
        'server': server,
        'host': host
    },
    'prosody': {
        'host': 'localhost',
        'port': 5782
    },
    'collect_vhost_users': True,
    'debug': False
}

def zabbix_write(item, value):
    send_str = f"{config['zabbix']['host']} {item} {value}"
    proc = Popen([config['zabbix']['sender'], '--zabbix-server', config['zabbix']['server'], '--input-file', '-'], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
    output = proc.communicate(input=send_str.encode())[0]
    if config['debug']:
        print(send_str)
        print(output.decode())

async def main():
    host = config['prosody']['host']
    port = config['prosody']['port']

    try:
        reader, writer = await asyncio.open_connection(host, port)
        writer.write(b"\n\n")
        await writer.drain()
        data = await reader.readuntil(b"\n\n")
        telnet_response = data.decode().split("\n")

        for line in telnet_response:
            elements = line.split(" ")
            if len(elements) > 2:
                key = elements[1].replace('"', '')
                if key == 'version':
                    value = elements[2].replace('(', '').replace(')', '').replace('"', '')
                else:
                    value = int(elements[2].replace('(', '').replace(')', '').replace('"', ''))
                key = "prosody." + key
                zabbix_write(key, value)

        writer.close()
        await writer.wait_closed()

    except Exception as e:
        print(f"Failed to connect or retrieve data: {e}")

    if config['collect_vhost_users']:
        base_dir = os.environ.get('internal_storage_path', "/var/lib/prosody")
        if os.path.isdir(base_dir):
            vhosts = listdirs(base_dir)
            for vhost in vhosts:
                account_dir = os.path.join(base_dir, vhost, "accounts")
                if os.path.isdir(account_dir):
                    vhost = urllib.parse.unquote(vhost)
                    munin_var = vhost.replace(".", "_")
                    accounts = len(list(listfiles(account_dir)))
                    zabbix_write(f"prosody.{munin_var}.users", accounts)

def listdirs(folder):
    for x in os.listdir(folder):
        if os.path.isdir(os.path.join(folder, x)):
            yield x

def listfiles(folder):
    for x in os.listdir(folder):
        if os.path.isfile(os.path.join(folder, x)):
            yield x

if __name__ == '__main__':
    asyncio.run(main())

