#!usr/bin/env python

import requests as req
import json
import sys
import time 
import datetime
from urllib.request import ssl, socket

site = (sys.argv[2])
arg = (sys.argv[1])
port = 443
certExpires = "none"
daysToExpiration = "none"
def score():
    url = ("https://http-observatory.security.mozilla.org/api/v1/analyze?host={}".format(site))
    res = req.get(url)
    g = (res.json().get("grade"))
    if g is None:
        rescan = req.post(url, data={'rescan': 'true', 'hidden': 'true'})
        def waitUntil():
            r = req.get(url)
            while (r.json().get("state") != 'FINISHED'):
                r = req.get(url)
                time.sleep(5)
        waitUntil()
    res = req.get(url)
    g = (res.json().get("grade"))
    print("score "+g)

def ltp():
    context = ssl.create_default_context()
    
    with socket.create_connection((site, port)) as sock:
        with context.wrap_socket(sock, server_hostname = site) as ssock:
            certificate = ssock.getpeercert()

    certExpires = datetime.datetime.strptime(certificate['notAfter'], '%b %d %H:%M:%S %Y %Z')
    daysToExpiration = (certExpires - datetime.datetime.now()).days

    ltp.expiredate = certExpires
    ltp.expiredays = daysToExpiration

def url_ok():
    r = req.get("https://"+site)
    print(r.status_code)
    
if arg == 'score':
    score()
if arg == 'ltpdate':
    ltp()
    print(ltp.expiredate)
if arg == 'ltpday':
    ltp()
    print(ltp.expiredays)
if arg == 'up':
    url_ok()
if arg == 'all':
    print("Site diagnostics for: "+site)
    print(site)
    print("Score at Mozilla observatory:")
    score()
    ltp()
    print("Days in which Letsencrypt cert expires:") 
    print(ltp.expiredays)
    print(ltp.expiredate)
    print(site+" return code:")
    url_ok()
    print("=============")
