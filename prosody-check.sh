#!/bin/bash
#This script checks the number and total of uploaded files on prosody
source /var/local/container-scripts/zabbix-scripts.cfg

#Number
PROSODY_TOTAL=$(sudo -u ${PROSODY_USER} ls -lh ${PROSODY_PATH} | wc -l)

#Total
PROSODY_SIZE=$(sudo -u  ${PROSODY_USER} du -bs ${PROSODY_PATH} | cut -f1)

#save onto a file
echo "prosody_total ${PROSODY_TOTAL}" > /tmp/prosody_stats
echo "prosody_size ${PROSODY_SIZE}" >> /tmp/prosody_stats
