#!/bin/bash
## Do not forget to install bc with `apt install bc`

# Number of users
DOVECOT_USER_COUNT=$(doveadm who |wc -l)

# Number of devices
DOVECOT_DEVICE_COUNT=$(doveadm who |awk '{print $2}' | sed '/^#/d' | paste -sd+ | bc)

#save to a file
echo "dovecot_usercount ${DOVECOT_USER_COUNT}" > /tmp/dovecot_stats
echo "dovecot_devicecount ${DOVECOT_DEVICE_COUNT}" >> /tmp/dovecot_stats