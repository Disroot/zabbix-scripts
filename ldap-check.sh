#!/bin/bash

source /var/local/container-scripts/ldap.cfg
TOTAL_ACTIVE=$(ldapsearch -b "ou=${OU_USER},${LDAP_BASE_DN}" -D "${LDAP_ADMIN_DN}" -y ${PASSFILE} |grep "dn: " |wc -l)
TOTAL_DEL=$(ldapsearch -b "ou=${DEL_OU},${LDAP_BASE_DN}" -D "${LDAP_ADMIN_DN}" -y ${PASSFILE} |grep "dn: " |wc -l)
TOTAL_BLOCKED=$(ldapsearch -b "ou=${BLOCK_OU},${LDAP_BASE_DN}" -D "${LDAP_ADMIN_DN}" -y ${PASSFILE} |grep "dn: " |wc -l)
STAT_FILE="/tmp/ldap_stats"

# post to file
echo "active ${TOTAL_ACTIVE}" > "${STAT_FILE}"
echo "deleted ${TOTAL_DEL}" >> "${STAT_FILE}"
echo "blocked ${TOTAL_BLOCKED}" >> "${STAT_FILE}"

