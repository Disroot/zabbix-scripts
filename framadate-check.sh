#!/bin/bash
#This script checks the amount of polls
source /var/local/container-scripts/zabbix-scripts.cfg

#all polls
POLLS_ALL=$(mysql -h "${POLLDB_HOST}" -u "${POLLDB_USER}" -p"${POLLDB_PASSWD}" "${POLLDB_NAME}" -s -r -N -e "SELECT COUNT(*) from ${POLLDB_PREFIX}poll;")

#poll type event
POLLS_EVENTS=$(mysql -h "${POLLDB_HOST}" -u "${POLLDB_USER}" -p"${POLLDB_PASSWD}" "${POLLDB_NAME}" -s -r -N -e "SELECT COUNT(*) from ${POLLDB_PREFIX}poll where format = 'D';")

#poll type standard
POLLS_STANDARD=$(mysql -h "${POLLDB_HOST}" -u "${POLLDB_USER}" -p"${POLLDB_PASSWD}" "${POLLDB_NAME}" -s -r -N -e "SELECT COUNT(*) from ${POLLDB_PREFIX}poll where format = 'A';")

#save onto a file
echo "polls_all ${POLLS_ALL}" > /tmp/framadate_stats
echo "polls_events ${POLLS_EVENTS}" >> /tmp/framadate_stats
echo "polls_standard ${POLLS_STANDARD}" >> /tmp/framadate_stats
