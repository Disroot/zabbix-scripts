
#!/bin/bash
#This script checks the amount of polls
source /var/local/container-scripts/zabbix-scripts.cfg

#all files
LUFI_ALLFILES=$(psql postgresql://${LUFIDB_USER}:${LUFIDB_PASSWD}@${LUFIDB_HOST}:${LUFIDB_PORT}/${LUFIDB_NAME} -t -c "SELECT COUNT(filename) FROM files WHERE filename IS NOT NULL;")
LUFI_DELETED=$( psql postgresql://${LUFIDB_USER}:${LUFIDB_PASSWD}@${LUFIDB_HOST}:${LUFIDB_PORT}/${LUFIDB_NAME} -t -c "SELECT COUNT(filename) FROM files WHERE filename IS NOT NULL and deleted = '1';")
LUFI_ACTIVE=$( psql postgresql://${LUFIDB_USER}:${LUFIDB_PASSWD}@${LUFIDB_HOST}:${LUFIDB_PORT}/${LUFIDB_NAME} -t -c "SELECT COUNT(filename) FROM files WHERE filename IS NOT NULL and deleted = '0';")
LUFI_SIZE=$(du -bs ${LUFI_PATH} | cut -f1)
echo "lufi_allfiles ${LUFI_ALLFILES}" > /tmp/lufi_stats
echo "lufi_deleted ${LUFI_DELETED}" >> /tmp/lufi_stats
echo "lufi_active ${LUFI_ACTIVE}" >> /tmp/lufi_stats
echo "lufi_size ${LUFI_SIZE}" >> /tmp/lufi_stats

